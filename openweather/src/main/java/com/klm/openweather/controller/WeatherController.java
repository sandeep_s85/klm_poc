package com.klm.openweather.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.klm.openweather.service.WeatherService;

@RestController
@RequestMapping("/")
public class WeatherController {
	
	@Value("${access.denied.exception}")
	private String accessDeniedException;

	private final WeatherService weatherService;

	@Autowired
	public WeatherController(WeatherService weatherService) {
		super();
		this.weatherService = weatherService;
	}
	
	private static final Logger log = LoggerFactory.getLogger(WeatherController.class);

	@RequestMapping(value = "/openweather", method = RequestMethod.GET)
	public @ResponseBody String getWeatherDetails(@RequestHeader(value="requestAllowed", defaultValue="true") boolean requestAllowed) {
		if(requestAllowed) {
			log.info("requestAllowed header is true");
			return weatherService.getWeatherDetails();
		}
		else {
			log.info("requestAllowed header is false");
			throw new AccessDeniedException(accessDeniedException);
		}
			
	}
}
