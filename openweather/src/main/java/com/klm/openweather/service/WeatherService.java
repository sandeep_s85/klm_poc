package com.klm.openweather.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.klm.openweather.utils.Utils;

@Service
public class WeatherService {	
	
	@Value("${authorization}")
	private String authorization;
	
	@Value("${bearer}")
	private String bearer;
	
	@Value("${endpoint}")
	private String endpoint;

	private OAuthTokenService oAuthTokenService;

	@Autowired
	public void setoAuthTokenService(OAuthTokenService oAuthTokenService) {
		this.oAuthTokenService = oAuthTokenService;
	}
	
	private static final Logger log = LoggerFactory.getLogger(WeatherService.class);

	private String restTemplate_Call(String endpoint) {		
		RestTemplate restTemplate = Utils.skipSSLVerification();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		StringBuilder accessToken = new StringBuilder();
		accessToken.append(bearer);
		accessToken.append(" ");
		accessToken.append(oAuthTokenService.getOAuth2Token());
		headers.set(authorization, accessToken.toString());
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<String> response = restTemplate.exchange(endpoint, HttpMethod.GET, entity, String.class);
		return response.getBody();
	}

	public String getWeatherDetails() {
		log.info("inside getWeatherDetails()");
		String response = restTemplate_Call(endpoint);
		log.info("Response : " + response);
		return response;
	}
}
