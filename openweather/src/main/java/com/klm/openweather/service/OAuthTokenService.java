package com.klm.openweather.service;


import org.apache.commons.codec.binary.Base64;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.klm.openweather.controller.WeatherController;
import com.klm.openweather.utils.Utils;

@Service
public class OAuthTokenService {	

	@Value("${token.uri}")
	private String tokenUri;
	
	@Value("${client.id}")
	private String clientId;
	
	@Value("${client.key}")
	private String clientKey;	
	
	@Value("${authorization}")
	private String authorization;	
	
	@Value("${basic}")
	private String basic;
	
	@Value("${grant.type}")
	private String grantType;
	
	@Value("${client.credentials}")
	private String clientCredentials;	
	
	@Value("${access.token}")
	private String accessToken;
	
	private static final Logger log = LoggerFactory.getLogger(OAuthTokenService.class);

	public String getOAuth2Token() {
		String responseBody = getAccessToken(tokenUri);
		String oAuthToken = extractAccessToken(responseBody);
		return oAuthToken;
	}

	private String getAccessToken(String tokenUri) {
		log.info("inside getAccessToken()");
		RestTemplate restTemplate = Utils.skipSSLVerification();		
		HttpHeaders headers = new HttpHeaders();
		StringBuilder accessToken = new StringBuilder();
		accessToken.append(basic);
		accessToken.append(" ");
		accessToken.append(Base64.encodeBase64String((clientId + ":" + clientKey).getBytes()));
		headers.add(authorization,accessToken.toString());
		log.info("Authorization : " + accessToken.toString());
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		log.info("Grant Type : " + grantType);
		log.info("Client Credentials : " + clientCredentials);
		map.add(grantType, clientCredentials);
		HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<MultiValueMap<String, String>>(map, headers);
		ResponseEntity<String> response = restTemplate.exchange(tokenUri, HttpMethod.POST, entity, String.class);
		log.info("Response : " + response.getBody());
		return response.getBody();
	}

	private String extractAccessToken(String responseBody) {
		JSONObject jsonResponse;
		String strAccessToken = null;
		try {
			jsonResponse = new JSONObject(responseBody);
			strAccessToken = jsonResponse.getString(accessToken);
			log.info("Access Token : " + strAccessToken);
		} catch (JSONException e) {
			e.printStackTrace();
		}	
		return strAccessToken;
	}	
}