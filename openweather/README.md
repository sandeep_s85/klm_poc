#OPEN WEATHER #

Use any Java framework to 

1. Consume a REST API 
	Access a REST endpoint https://klm.api.mashery.com/abn/openweather using oAuth 2 client credential method.
	Token url : https://klm.api.mashery.com/abn/oauth (If possible, try to use any oAuth client framework get get tokens)
	Client ID & Secret : #client_id#/#client_secret#

2. Expose a REST service which will give back a filtered response based on above call.
	Response exposed through a secured(any authentication) endpoint.
	Response filter criteria : should contain only the fields which are passed in the requestbody. (coord, weather, base, main, visibility etc are possible values in the given example)
	
	Give back the response only if "requestAllowed" header = true in the request. Else, give back "Not Authorized" response.
	

Sample calls
*************

Token creation : 
$ curl -X POST   https://klm.api.mashery.com/abn/oauth   -H 'authorization: Basic #encoded_credentials#'   -H 'content-type: application/x-www-form-urlencoded'   -d grant_type=client_credentials --insecure
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   156  100   127  100    29     86     19  0:00:01  0:00:01 --:--:--    88{"access_token":"#access_token#","token_type":"bearer","expires_in":86400,"refresh_token":"#refresh_token#"}


Resource call :
$ curl -X GET   https://klm.api.mashery.com/abn/openweather   -H 'authorization: Bearer #access_token#' --insecure
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   465  100   465    0     0    387      0  0:00:01  0:00:01 --:--:--   397{"coord":{"lon":-0.13,"lat":51.51},"weather":[{"id":310,"main":"Drizzle","description":"light intensity drizzle rain","icon":"09d"}],"base":"stations","main":{"temp":290.34,"pressure":1016,"humidity":77,"temp_min":287.15,"temp_max":292.15},"visibility":10000,"wind":{"speed":4.6,"deg":320},"clouds":{"all":75},"dt":1500909600,"sys":{"type":1,"id":5091,"message":0.0023,"country":"GB","sunrise":1500869619,"sunset":1500926372},"id":2643743,"name":"London","cod":200}